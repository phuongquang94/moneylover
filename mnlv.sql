USE [master]
GO
/****** Object:  Database [MoneyLover]    Script Date: 7/17/2015 4:07:25 PM ******/
CREATE DATABASE [MoneyLover]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MoneyLover', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.BO\MSSQL\DATA\MoneyLover.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MoneyLover_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.BO\MSSQL\DATA\MoneyLover_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MoneyLover] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MoneyLover].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MoneyLover] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MoneyLover] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MoneyLover] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MoneyLover] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MoneyLover] SET ARITHABORT OFF 
GO
ALTER DATABASE [MoneyLover] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MoneyLover] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [MoneyLover] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MoneyLover] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MoneyLover] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MoneyLover] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MoneyLover] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MoneyLover] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MoneyLover] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MoneyLover] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MoneyLover] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MoneyLover] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MoneyLover] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MoneyLover] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MoneyLover] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MoneyLover] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MoneyLover] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MoneyLover] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MoneyLover] SET RECOVERY FULL 
GO
ALTER DATABASE [MoneyLover] SET  MULTI_USER 
GO
ALTER DATABASE [MoneyLover] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MoneyLover] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MoneyLover] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MoneyLover] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'MoneyLover', N'ON'
GO
USE [MoneyLover]
GO
/****** Object:  Table [dbo].[CurrentMoneyInYourWallet]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrentMoneyInYourWallet](
	[UserID] [int] NOT NULL,
	[CurrentMoney] [numeric](18, 0) NULL,
	[CurrentNotice] [nvarchar](max) NULL,
 CONSTRAINT [PK_CurrentMoneyInYourWallet] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Expense]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expense](
	[UserID] [int] NOT NULL,
	[ExpenseMoney] [numeric](18, 0) NULL,
	[ExpenseDate] [datetime] NULL,
	[ExpenseReason] [nvarchar](max) NULL,
	[ExpenseNotice] [nvarchar](max) NULL,
 CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Income]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Income](
	[UserID] [int] NOT NULL,
	[IncomeMoney] [numeric](18, 0) NULL,
	[IncomeDate] [datetime] NULL,
	[IncomeNotice] [nvarchar](max) NULL,
 CONSTRAINT [PK_Income] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Owe]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Owe](
	[UserID] [int] NOT NULL,
	[OweMoney] [numeric](18, 0) NULL,
	[OweCreditor] [nvarchar](max) NULL,
	[OweDate] [datetime] NULL,
	[OwePeriod] [datetime] NULL,
	[OweNotice] [nvarchar](max) NULL,
 CONSTRAINT [PK_Owe] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Saving]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Saving](
	[UserID] [int] NOT NULL,
	[SavingMoney] [numeric](18, 0) NULL,
	[SavingDate] [datetime] NULL,
	[SavingMethod] [nvarchar](max) NULL,
	[SavingPlace] [nvarchar](max) NULL,
	[SavingNotice] [nvarchar](max) NULL,
 CONSTRAINT [PK_Saving] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Total]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Total](
	[UserID] [int] NOT NULL,
	[TotalMoney] [numeric](18, 0) NULL,
	[TotalOwe] [numeric](18, 0) NULL,
	[TotalNotice] [nvarchar](max) NULL,
 CONSTRAINT [PK_Total] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 7/17/2015 4:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] NOT NULL,
	[UserName] [varchar](100) NULL,
	[UserPassword] [varchar](200) NULL,
	[UserCreateDate] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [MoneyLover] SET  READ_WRITE 
GO
